# Base container for PHP7 Projects #

## PHP 7.0 ##
[Dotdeb.org](http://www.dotdeb.org/) is a repository containing packages to turn your Debian boxes into powerful, stable and up-to-date LAMP servers.

## Common ##
[Compser](https://getcomposer.org/) Dependency Manager for PHP
[PHPUnit](https://phpunit.de/) PHPUnit is a programmer-oriented testing framework for PHP
