# Debian Wheezy Container for PHP 5.6
#
# Includes: Composer and BlackFire.io
#
# Version   1.0.3

FROM        mariorez/debian:wheezy
MAINTAINER  Mario Rezende <mariorez@gmail.com>

RUN echo "deb http://packages.dotdeb.org wheezy all" > /etc/apt/sources.list.d/dotdeb.list \
    && echo "deb-src http://packages.dotdeb.org wheezy all" >> /etc/apt/sources.list.d/dotdeb.list \
    && echo "deb http://packages.dotdeb.org wheezy-php56 all" >> /etc/apt/sources.list.d/dotdeb.list \
    && echo "deb-src http://packages.dotdeb.org wheezy-php56 all" >> /etc/apt/sources.list.d/dotdeb.list \
    && echo "deb http://packages.blackfire.io/debian any main" > /etc/apt/sources.list.d/blackfire.list \
    && wget -O - http://www.dotdeb.org/dotdeb.gpg | apt-key add - \
    && wget -O - https://packagecloud.io/gpg.key | apt-key add -

RUN packages=" \
        blackfire-agent \
        blackfire-php \
        php5-cli \
        php5-curl \
        php5-fpm \
        php5-intl \
        php5-json \
        php5-mcrypt \
        php5-mysql \
        php5-sqlite \
    " \
    && set -x \
    && apt-get update && apt-get install -y $packages

    # PHP custom configuration
RUN sed -i "s/;date.timezone =/date.timezone = America\/Sao_Paulo/" /etc/php5/cli/php.ini \
    && sed -i "s/;date.timezone =/date.timezone = America\/Sao_Paulo/" /etc/php5/fpm/php.ini \
    && sed -i "s/www-data/docker/g" /etc/php5/fpm/pool.d/www.conf \
    && sed -i "s/listen = \/var\/run\/php5-fpm.sock/listen = 9000/" /etc/php5/fpm/pool.d/www.conf

    # Install Composer globally
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

VOLUME /var/www

EXPOSE 9000

ENTRYPOINT ["php5-fpm", "--nodaemonize"]
