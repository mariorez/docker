FROM alpine:latest

# Timezone
ENV TIMEZONE America/Sao_Paulo

RUN echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk update && \
    apk upgrade && \
    apk add --update tzdata && \
    cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && \
    echo "${TIMEZONE}" > /etc/timezone && \
    apk add --update \
        curl \
        git \
        openssh-client \
        php \
        php-curl \
        php-fpm \
        php-intl \
        php-json \
        php-ldap \
        php-openssl \
        php-phar \
        php-xdebug@testing

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

RUN mkdir -p /var/www  && \
    adduser -u 1000 www-data -D -s /bin/ash && \
    chown -R www-data:www-data /var/www && \
    sed -i "s|;date.timezone =.*|date.timezone = ${TIMEZONE}|" /etc/php/php.ini && \
    echo "zend_extension=xdebug.so"      > /etc/php/conf.d/xdebug.ini && \
    echo "xdebug.max_nesting_level=250" >> /etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_enable=1"       >> /etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_handler=dbgp"   >> /etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_mode=req"       >> /etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_port=9000"      >> /etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_autostart=1"    >> /etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_connect_back=1" >> /etc/php/conf.d/xdebug.ini

RUN apk del tzdata && \
    rm -fr /tmp/*.apk && \
    rm -rf /var/cache/apk/*

VOLUME /var/www

EXPOSE 9000

ENTRYPOINT ["php5-fpm", "--nodaemonize"]
